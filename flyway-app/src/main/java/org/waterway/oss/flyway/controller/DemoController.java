package org.waterway.oss.flyway.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

/**
 * Demo controller
 *
 * @author yongqiang
 * @date 2017/11/08
 */
@RestController
@RequestMapping("/v1/demo")
public class DemoController {

    private static final Logger logger = LoggerFactory.getLogger(DemoController.class);


    /**
     * 查询用户
     *
     * @return
     */
    @GetMapping()
    public String getUser(@PathVariable("id") Integer id) {
        return "flywaydb demo";
    }





}
