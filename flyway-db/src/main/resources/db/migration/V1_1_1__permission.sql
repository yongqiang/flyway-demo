/*
Navicat MySQL Data Transfer

Source Server         : mysql
Source Server Version : 50623
Source Host           : localhost:3306
Source Database       : devopsone_core

Target Server Type    : MYSQL
Target Server Version : 50623
File Encoding         : 65001

Date: 2017-11-02 19:05:33
*/

SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id`                INT(11) NOT NULL AUTO_INCREMENT,
  `name`              VARCHAR(255)     DEFAULT NULL
  COMMENT '角色名称',
  `permission`        VARCHAR(255)     DEFAULT NULL,
  `state`             VARCHAR(255)     DEFAULT NULL,
  `permission_target` INT(11)          DEFAULT NULL
  COMMENT '权限级别',
  `create_user`       INT(11)          DEFAULT NULL
  COMMENT '创建人',
  `create_time`       DATETIME         DEFAULT NULL
  COMMENT '创建时间',
  `update_user`       INT(11)          DEFAULT NULL
  COMMENT '更新人',
  `update_time`       DATETIME         DEFAULT NULL
  COMMENT '更新时间',
  `remark`            VARCHAR(255)     DEFAULT NULL
  COMMENT '备注',
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
